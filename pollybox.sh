#!/bin/bash

VOLUME='/Volumes/Music'

if [ ! -d $VOLUME ];then
  echo $VOLUME does not exists 1>&2
  exit 1
fi

echo "Give me a name:"
read name
echo "Give me a text to say:"
read text

mkdir -p "$VOLUME/$name" && cd "$VOLUME/$name" && aws polly synthesize-speech --text "$text" --output-format mp3 --voice-id Celine outfile.mp3 --region eu-west-1
